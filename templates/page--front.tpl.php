 
  <div class="container hidden-phone search">
   <div class="row">
          <?php if($page['top']): ?>
             <div class="span4" id="search">

                <?php print render($page['top']); ?>

             </div>
          <?php endif; ?>  
   </div>
  </div>
  
   <div class="container menu-bg">
      <div class="row">
      <!--Logo-->
         <div class="span3">
            <div class="logo">
              <a href="<?php print $front_page;?>">
                <img src="<?php echo base_path() . $directory; ?>/logo.png" alt="<?php print $site_name;?>" id="logo"/>
              </a>
            </div>
          
     
     </div><!--logo-->


      
      <!--Navigation-->
     
         <div class="span9">
       <a class="toggleMenu" href="#">Navigation</a>

        
        <?php 
          if (module_exists('i18n')) {
            $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
          } else {
            $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
          }
          print drupal_render($main_menu_tree);
        ?>
      </div><!--top-menu-->
     
         </div>
        
       
      
    </div><!--container1-->  
      
     <div class="container container-bg">
      <div class="row">  
      <div class="span12">
            <!--Slideshow-->
          <div id="slideshow" class="hidden-phone">
                 <?php if($page['slideshow']): ?>
                      <?php print render($page['slideshow']); ?>
                  <?php endif; ?>  
          </div>
      </div>
       <div class="span8">
         
        <!--mainbody-->             
             
               <!--user1 position-->
             <div class="hidden-phone">  
               <div class="row">   
                
                  <div class="span4">
                       <?php if($page['user1']): ?>
                          
                             <?php print render($page['user1']); ?>
  
                       <?php endif; ?>   
                  </div><!--span4-->
                  
                  <div class="span4">
                  <?php if($page['user2']): ?>

                          <?php print render($page['user2']); ?>

                   <?php endif; ?>   
             	  </div><!--span4-->
                  
               </div><!--row-->
             </div>  
              <div class="bottom-border"></div>
             
             <!--content-->
             <div class="row">
             
                 <div class="span5" id="contentarea" >
                  <?php if($page['content']): ?>
                                
                              <?php print render($page['content']); ?>
      
                  <?php endif; ?>                            
                 </div><!--span5-->
                 <div class="hidden-tablet">
                 <div class="span3">
                  <div id="sidebar-left">
                    <?php if($page['sidebar_left']): ?>
    
                         <?php print render($page['sidebar_left']); ?>
      
                      <?php endif; ?>       
                   </div><!--sidebar-left-->
                 </div><!--span3-->
             </div>
             </div><!--row content-->
               
             </div><!--span8-->
             
             <!--sidebar right-->
             <div class="span4">
               <div id="sidebar-right">
               <?php if($page['sidebar_right']): ?>
    
                         <?php print render($page['sidebar_right']); ?>
      
                 <?php endif; ?>     
               </div>
             </div><!--span4-->
          
      </div><!--row2-->
      <div class="bottom-border"></div>
      
      <div class="row">
       <div id="bottom">
       
         <div id="user4" class="span3">
             <?php if($page['user3']): ?>
    
                  <?php print render($page['user3']); ?>
      
               <?php endif; ?> 
         </div><!--span3-->
         
         <div id="user5" class="span3">
            <?php if($page['user4']): ?>
    
                  <?php print render($page['user4']); ?>
      
               <?php endif; ?> 
         </div><!--span3-->
         
         <div id="user6" class="span3">
             <?php if($page['user5']): ?>
    
                  <?php print render($page['user5']); ?>
      
               <?php endif; ?> 
         </div><!--span3-->
         
         <div id="user7" class="span3">
            <?php if($page['user6']): ?>
    
                  <?php print render($page['user6']); ?>
      
               <?php endif; ?> 
         </div><!--span3-->
         
       </div><!--bottom-->  
      </div><!--row-->
    </div><!--container2-->
      
    <div class="container footer-bg">
      <div id="footer">
         <?php if($page['footer']): ?>
    
             <?php print render($page['footer']); ?>
      
         <?php endif; ?> 
         
         Copyright &copy; <?php echo DATE('Y'); ?>. <a href="<?php print $front_page;?>"><?php print $site_name;?></a>. All Rights Reserved.
         
      </div><!--footer-->
    </div><!--container-->
    