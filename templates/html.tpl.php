<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,width=device-width" />
<meta name="HandheldFriendly" content="true" />
<meta name="MobileOptimized" content="width=320px,user-scalable=no" />
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<link rel="stylesheet" href="<?php echo base_path() . $directory; ?>/css/style.css" />
<link rel="stylesheet" href="<?php echo base_path() . $directory; ?>/css/bootstrap.css"/>
<link rel="stylesheet" href="<?php echo base_path() . $directory; ?>/css/bootstrap-responsive.css"/>

 <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<body>
<div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
  
<script src="<?php echo base_path() . $directory; ?>/js/jquery.js"></script>
<script src="<?php echo base_path() . $directory; ?>/js/bootstrap.js"></script>
<script src="<?php echo base_path() . $directory; ?>/js/bootstrap-collapse.js"></script>
<script src="<?php echo base_path() . $directory; ?>/js/respond.min.js"></script>
<script src="<?php echo base_path() . $directory; ?>/js/respond.src.js"></script>



<script  type="text/javascript">

$('.slides li:gt(0)').hide();
$('.slides li:last').addClass('last');
var cur = $('.slides li:first');

function animate() {
	cur.fadeOut( 1000 );
	if ( cur.attr('class') == 'last' )
		cur = $('.slides li:first');
	else
		cur = cur.next();
	cur.fadeIn( 1000 );
}

$(function() {
	setInterval( "animate()", 5000 );
} );



</script>


<script src='http://use.edgefonts.net/open-sans.js'></script>


</body>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_path() . $directory; ?>/js/script.js"></script>

</html>
